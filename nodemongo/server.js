const os = require('os')

const express = require('express')
const app = express()
const bodyParser = require('body-parser')


const mongo = require('mongodb').MongoClient
var ObjectId = require('mongodb').ObjectID;
const uris = [
  'mongodb://localhost:27017',
  'mongodb+srv://srlopez:Pa88word@cluster0-jktlv.azure.mongodb.net/test?retryWrites=true&w=majority',
  'mongodb://sa:Pa88word@localhost:27017'
]
const uri = 1
const dbname = 'mydb'
const colname = 'users'

// la bd y el server
const client = new mongo(uris[uri], {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var db
var server

client.connect(err => {
  if (err) {
    console.error(err)
    return
  }
  db = client.db(dbname)
  console.log(client.s.options.dbName + '@' + client.s.options.srvHost)

  // Establecemos el server
  server = app.listen(process.env.PORT || 3000, () => {
    console.log('listening on ' + server.address().port)
  })
})

app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())
app.use(express.static('public'))

// ==========  ADORNOS ===========
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

app.get('/ping', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({
    hostname: os.hostname(),
    //ip: server.address().address,
    port: server.address().port,
    dbName: client.s.options.dbName,
    srvHost: client.s.options.srvHost
  }))
})

app.get('/html', (req, res) => {
  db.collection(colname).find().toArray((err, result) => {
    if (err) return console.log(err)
    res.render('index.ejs', {
      users: result
    })
  })
})

// =========  API ===========
// https://docs.mongodb.com/manual/crud/
// SELECT ID
app.get('/users/:id', (req, res) => {
  console.log('get ' + req.params.id)

  db.collection('users').findOne({
    _id: ObjectId(req.params.id)
  }, (err, item) => {
    console.log(err || item)
    if (err) return res.send(500, err)
    //console.log(item)
    res.send(item)
  })
})

// SELECT ALL
app.get('/users', (req, res) => {
  db.collection(colname).find().toArray((err, result) => {
    if (err) return console.log(err)
    res.send(result)
  })
})

// INSERT
app.post('/users', (req, res) => {
  console.log('post')
  console.log(req.body)

  db.collection(colname).insertOne(req.body, (err, result) => {
    console.log(err || result.result)
    if (err) return console.log(err)
    res.send(result)
  })
})

// UPDATE
app.put('/users', (req, res) => {
  console.log('put')
  console.log(req.body)

  db.collection('users').findOneAndUpdate(
    //db.collection('users').updateOne(
    {
      _id: ObjectId(req.body._id)
    }, {
      $set: {
        name: req.body.name,
        email: req.body.email
      }
    },
    //{ sort: {_id: -1}, upsert: true }, 
    (err, response) => {
      console.log(err || response)
      if (err) return res.send(err)
      res.send(response)
    })
})

// DELETE 
app.delete('/users/:id', (req, res) => {
  console.log('delete ' + req.params.id)

  //db.collection(colname).findOneAndDelete({ _id: ObjectId(req.params.id) }, (err, item) => {
  db.collection(colname).deleteOne({
    _id: ObjectId(req.params.id)
  }, (err, item) => {
    console.log(err || item.result)
    if (err) return res.send(500, err)
    //console.log(item)
    res.send(item)
  })
})